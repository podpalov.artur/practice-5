import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        char[] array = new char[]{333, 7, 4, 9, 10, 421, 8754, 12, 1, 90};
        char[] backuparr = Arrays.copyOf(array, 10);
        SelectionSort.sort(array);
        printarr(array);
        array = Arrays.copyOf(backuparr, 10);
        InsertionSort.sort(array);
        printarr(array);
        array = Arrays.copyOf(backuparr, 10);
        BubbleSort.sort(array);
        printarr(array);
    }

    public static void printarr(char[] array) {
        for (int element : array) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}

class SelectionSort {
    public static void sort(char[] array) {
        System.out.println("Selection sort: ");
        int pos;
        for (int i = 0; i < array.length; i++) {
            pos = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[pos]) {
                    pos = j;
                }
            }
            char temp = array[pos];
            array[pos] = array[i];
            array[i] = temp;
        }
    }
}

class InsertionSort {
    public static void sort(char[] array) {
        System.out.println("Insertion sort: ");
        for (int i = 1; i < array.length; i++) {
            char currElem = array[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && array[prevKey] > currElem) {
                array[prevKey + 1] = array[prevKey];
                array[prevKey] = currElem;
                prevKey--;
            }
        }
    }
}

class BubbleSort {
    public static void sort(char[] array) {
        System.out.println("Bubble sort: ");
        boolean isSorted = false;
        char buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;
                }
            }
        }
    }
}